import csv
from get_matches_id import get_matches_id

def extra_runs_team_2016(filepath):
    match_id = get_matches_id("matches.csv",2016)
    try:
        team_extra_runs = {}
        with open(filepath) as deliveries_csv:
            deliveries_file = csv.DictReader(deliveries_csv)
            for delivery in deliveries_file:
                if delivery['match_id'] in match_id:
                    team = delivery['bowling_team']
                    if team in team_extra_runs:
                        team_extra_runs[team] += int(delivery['extra_runs'])
                    else:
                        team_extra_runs[team] = int(delivery['extra_runs'])
        return team_extra_runs
    except Exception as e:
        print(e.args)
        return type(e)

