import unittest
from extra_runs_2016 import extra_runs_team_2016

class TestExtraRunsTeam2016(unittest.TestCase):
    def test_extra_runs_team_2016(self):
        self.assertEqual(extra_runs_team_2016("2012_deliveries.csv"),FileNotFoundError)
        self.assertEqual(extra_runs_team_2016("2016_deliveries _corrupt.csv"),ValueError)
        self.assertEqual(extra_runs_team_2016("2016_deliveries.csv"),{'Delhi Daredevils': 9, 'Sunrisers Hyderabad': 5, 'Gujarat Lions': 4})


if __name__ == "__main__":
    unittest.main()