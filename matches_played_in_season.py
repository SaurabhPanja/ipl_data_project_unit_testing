import csv
def matches_played_in_season(filepath, year):
    try:
        year = str(int(year))
        count = 0
        year = str(year)
        with open(filepath) as matches_csv:
            matches_file = csv.DictReader(matches_csv)
            for match in matches_file:
                if(match['season'] == year):
                    count += 1
        return count
    except Exception as e:
        print(e.args)
        return type(e)



