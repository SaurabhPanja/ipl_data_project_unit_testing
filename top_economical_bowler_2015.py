import csv
from get_matches_id import get_matches_id

def get_bowler_run_concede_2015(filepath):
    ids_of_matches_played_2015 = get_matches_id("matches.csv",2015)
    bowler_run_conceded_2015 = {}
    with open(filepath) as deliveries_csv:
        deliveries_csv_reader = csv.DictReader(deliveries_csv)
        for delivery in deliveries_csv_reader:
            if delivery['match_id'] in ids_of_matches_played_2015:
                bowler = delivery['bowler']
                wide_runs = int(delivery['wide_runs'])
                noball_runs = int(delivery['noball_runs'])
                batsmen_runs = int(delivery['batsman_runs'])
                total_runs_delivery = wide_runs + noball_runs + batsmen_runs
                if delivery['is_super_over'] == '1':
                    continue
                if bowler in bowler_run_conceded_2015:
                    if 'runs' in bowler_run_conceded_2015[bowler] and 'balls' in bowler_run_conceded_2015[bowler]:
                        bowler_run_conceded_2015[bowler]['runs'] += total_runs_delivery
                        bowler_run_conceded_2015[bowler]['balls'] += 1
                    else:
                        bowler_run_conceded_2015[bowler]['runs'] = total_runs_delivery
                        bowler_run_conceded_2015[bowler]['balls'] = 1

                    if wide_runs > 0 or noball_runs > 0:
                        bowler_run_conceded_2015[bowler]['balls'] -= 1
                else:
                    bowler_run_conceded_2015[bowler] = {
                        'runs': total_runs_delivery,
                        'balls': 1
                    }
                    if wide_runs > 0 or noball_runs > 0:
                        bowler_run_conceded_2015[bowler]['balls'] -= 1
    return bowler_run_conceded_2015


def get_economy_of_bowler_2015(bowler_run_conceded_2015):
    economy_of_bowler_2015 = {}
    for bowler in bowler_run_conceded_2015:
        economy = (bowler_run_conceded_2015[bowler]['runs']
                   * 6) / bowler_run_conceded_2015[bowler]['balls']
        economy_of_bowler_2015[bowler] = round(economy, 2)
    return economy_of_bowler_2015

def sort_economy_of_bowler(economy_of_bowler_2015):
    bowler_name, bowler_economy = [], []
    for key, value in economy_of_bowler_2015.items():
        bowler_name.append(key)
        bowler_economy.append(value)

    bowler_economy.sort()

    top_n_bowler = {}
    for economy in bowler_economy:
        for key, value in economy_of_bowler_2015.items():
            if value == economy:
                top_n_bowler[key] = value

    return top_n_bowler

def execute(filepath):
    try:
        return sort_economy_of_bowler(get_economy_of_bowler_2015(get_bowler_run_concede_2015(filepath)))
    except Exception as e:
        print(e.args)
        return type(e)