import unittest
from get_matches_id import get_matches_id

class TestGetMatchId(unittest.TestCase):
    def test_get_match_id(self):
        self.assertEqual(get_matches_id("doesntexist.csv","2016"),"Andha hai sale, dekh k type kar file name!")
        self.assertEqual(get_matches_id("2016_matches.csv","Two thousand sixteen"),"Abey integer aur strings mai farak nai dekhta tujhe!")
        self.assertEqual(get_matches_id(2016,"2016_matches.csv"),"Abey integer aur strings mai farak nai dekhta tujhe!")
        self.assertEqual(get_matches_id("2016_matches.csv","2016"), ['583', '584', '585'])
        self.assertEqual(get_matches_id("2015_matches.csv","2015"), ['547', '548'])


if __name__ == "__main__":
    unittest.main()