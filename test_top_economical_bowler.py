import unittest
from top_economical_bowler_2015 import execute
class TestEconomicalBowler(unittest.TestCase):
    def test_top_economical_bowler_2015(self):
        self.assertEqual(execute("doesntexist.csv"), FileNotFoundError)
        self.assertEqual(execute("2015_deliveries_corrupt.csv"), ValueError)
        self.assertEqual(execute("2015_deliveries.csv"),{'NM Coulter-Nile': 3.0, 'PJ Cummins': 3.43, 'JP Duminy': 5.0, 'A Mishra': 13.2, 'UT Yadav': 14.4})


if __name__ == "__main__":
    unittest.main()