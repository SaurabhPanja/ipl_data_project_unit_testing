import unittest
from won_matches_team_season import won_matches_team_season

class TestWonMatchesTeam(unittest.TestCase):
    def test_won_matches_season(self):
        self.assertEqual(won_matches_team_season("doesnotexist.csv","Sunrisers Hyderabad"),FileNotFoundError)
        self.assertEqual(won_matches_team_season("matches.csv","2008"), ValueError)
        self.assertEqual(won_matches_team_season("matches.csv", "Rising Challenger Bangalore"), TypeError)
        #true result
        self.assertEqual(won_matches_team_season("matches.csv", 'Sunrisers Hyderabad'),{'2013': 1, '2014': 4, '2015': 1, '2017': 1})
        self.assertEqual(won_matches_team_season("matches.csv", 'Rising Pune Supergiant'), {'2017': 1})
        self.assertEqual(won_matches_team_season("matches.csv", 'Kolkata Knight Riders'),  {'2017': 1, '2008': 1, '2010': 1, '2011': 1, '2012': 1, '2013': 1, '2014': 3, '2016': 1})
        self.assertEqual(won_matches_team_season("matches.csv", 'Kings XI Punjab'),  {'2017': 1, '2013': 1, '2014': 6})
        self.assertEqual(won_matches_team_season("matches.csv", "Royal Challengers Bangalore"),  {'2009': 1, '2010': 1, '2011': 1, '2012': 1, '2013': 2, '2014': 1, '2015': 1})
        self.assertEqual(won_matches_team_season("matches.csv", "Mumbai Indians"),  {'2009': 1, '2010': 1, '2011': 1, '2012': 3, '2013': 1, '2014': 3, '2015': 2})
        self.assertEqual(won_matches_team_season("matches.csv", "Delhi Daredevils"),  {'2009': 1, '2012': 2, '2014': 2, '2015': 1, '2016': 1})
        self.assertEqual(won_matches_team_season("matches.csv", "Gujarat Lions"),  {'2016': 1})
        self.assertEqual(won_matches_team_season("matches.csv", "Chennai Super Kings"),  {'2008': 2, '2009': 1, '2010': 2, '2011': 1, '2012': 2, '2013': 1, '2014': 7, '2015': 1})
        self.assertEqual(won_matches_team_season("matches.csv", "Rajasthan Royals"),  {'2008': 3, '2011': 2, '2012': 2, '2013': 1, '2014': 5})
        self.assertEqual(won_matches_team_season("matches.csv", "Deccan Chargers"),{'2009': 1, '2010': 1})
        self.assertEqual(won_matches_team_season("matches.csv", "Kochi Tuskers Kerala"),{})
        self.assertEqual(won_matches_team_season("matches.csv", "Rising Pune Supergiants"),{})



if __name__ == "__main__":
    unittest.main()