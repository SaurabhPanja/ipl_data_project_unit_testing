import unittest
from matches_played_in_season import matches_played_in_season

class TestMatchesPlayedPerSeason(unittest.TestCase):

    def test_matches_played_season(self):
        self.assertEqual(matches_played_in_season("do_not_exist.csv", 2017), FileNotFoundError)
        self.assertEqual(matches_played_in_season("", 2017),FileNotFoundError)
        self.assertEqual(matches_played_in_season("matches.csv", "Two Thousand Eight"),ValueError)
        self.assertEqual(matches_played_in_season(2017, "matches.csv"), ValueError)
        self.assertEqual(matches_played_in_season(2017, 2018), OSError)
        self.assertEqual(matches_played_in_season("matches.txt",""), ValueError)
        self.assertEqual(matches_played_in_season("matches.csv",2017), 4)
        self.assertEqual(matches_played_in_season("matches.csv",2008), 6)
        self.assertEqual(matches_played_in_season("matches.csv",2009), 5)
        self.assertEqual(matches_played_in_season("matches.csv",2010), 6)
        self.assertEqual(matches_played_in_season("matches.csv",2011), 7)
        self.assertEqual(matches_played_in_season("matches.csv",2012), 13)
        self.assertEqual(matches_played_in_season("matches.csv",2013), 10)
        self.assertEqual(matches_played_in_season("matches.csv",2014), 31)
        self.assertEqual(matches_played_in_season("matches.csv",2015), 7)
        self.assertEqual(matches_played_in_season("matches.csv",2016), 3)

        
        
if __name__ == "__main__":
    unittest.main()