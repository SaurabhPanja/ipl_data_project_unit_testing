import csv

def won_matches_team_season(filepath, team):
    team_list = ['Sunrisers Hyderabad', 'Rising Pune Supergiant', 'Kolkata Knight Riders', 'Kings XI Punjab', 'Royal Challengers Bangalore', 'Mumbai Indians', 'Delhi Daredevils', 'Gujarat Lions', 'Chennai Super Kings', 'Rajasthan Royals', 'Deccan Chargers', 'Pune Warriors', 'Kochi Tuskers Kerala', 'Rising Pune Supergiants']
    try:
        if filepath.isnumeric() or team.isnumeric():
            raise ValueError
        if team not in team_list:
            raise "Team doesn't exist"
        matches_won_in_year = {}
        with open(filepath) as matches_csv:
            matches_file = csv.DictReader(matches_csv)
            for match in matches_file:
                year = match['season']
                if(match['winner'] == team):
                    if year in matches_won_in_year:
                        matches_won_in_year[year] += 1
                    else:
                        matches_won_in_year[year] = 1

        return matches_won_in_year
    except Exception as e:
        print(e.args)
        return type(e)
