import csv

def get_matches_id(filepath, year):
    id_list = []
    try:
        year = str(int(year))
        with open(filepath) as matches_csv:
            matches_file = csv.DictReader(matches_csv)
            for match in matches_file:
                if match['season'] == year:
                    id_list.append(match['id']) 
    except FileNotFoundError:
        return "Andha hai sale, dekh k type kar file name!"
    except ValueError:
        return "Abey integer aur strings mai farak nai dekhta tujhe!"

    return id_list